package cGuest

import (
	"mvcgolang/controller"
	"mvcgolang/model/mUser"
	"mvcgolang/utils"
	"net/http"
)

func CreateAccount(ctx *controller.Ctx) {
	account := mUser.Account{}
	err := ctx.ShouldBindJSON(&account)
	res := map[string]interface{}{}
	if err == nil {
		pass := utils.HashGenerator(account.Password)
		account.Password = pass
		// err = account.InsertNewAccount(db *gorm.DB)
		err = account.InsertNewAccount(ctx.Db)
		if err == nil {
			res["account"] = account
		}
	}
	if err != nil {
		res[`error`] = err.Error()
	}
	ctx.JSON(http.StatusOK, res)
}

func Login(ctx *controller.Ctx) {
	auth := mUser.Auth{}
	err := ctx.ShouldBindJSON(&auth)
	res := map[string]interface{}{}
	if err == nil {
		token := ""
		err, token = auth.Login(ctx.Db)
		if err == nil {
			res["token"] = token
		}
	}
	if err != nil {
		res[`error`] = err.Error()
	}
	ctx.JSON(http.StatusOK, res)
}
