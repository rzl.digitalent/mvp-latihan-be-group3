package cUser

import (
	"fmt"
	"mvcgolang/controller"
	"mvcgolang/middleware"
	"mvcgolang/model/mUser"
	"net/http"
)

func checkLogin(ctx *controller.Ctx) (map[string]interface{}, int, bool) {
	res := map[string]interface{}{}
	idAccount := middleware.Auth(ctx.Context)
	if idAccount < 0 {
		res[`error`] = `not yet logged in`
		ctx.JSON(http.StatusOK, res)
		return nil, 0, true
	}
	return res, idAccount, false
}

func Account(ctx *controller.Ctx) {
	res, idAccount, done := checkLogin(ctx)
	if done {
		return
	}
	account := mUser.Account{}
	account.ID = idAccount
	err := account.GetAccountDetail(ctx.Db)
	fmt.Printf("%#v\n", account)
	if err == nil {
		res[`account`] = account
	}
	if err != nil {
		res[`error`] = err.Error()
	}
	ctx.JSON(http.StatusOK, res)
}
