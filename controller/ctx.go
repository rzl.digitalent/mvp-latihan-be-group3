package controller

import "github.com/gin-gonic/gin"

type Ctx struct {
	*gin.Context
	*Config
}

type Handler func(*Ctx)
