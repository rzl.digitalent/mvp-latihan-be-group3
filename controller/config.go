package controller

import (
	"log"
	"math/rand"
	"mvcgolang/model/mUser"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const (
	MysqlDsn = "root:root@/be02?charset=utf8&parseTime=True&loc=Local"
)

type Config struct {
	Router *gin.Engine
	Db     *gorm.DB
}

func Init() *Config {
	rand.Seed(time.Now().UnixNano())

	db, err := gorm.Open(mysql.Open(MysqlDsn), &gorm.Config{})
	if err != nil {
		log.Fatalf(`failed to connect to mysql: ` + err.Error())
	}
	db.AutoMigrate(&mUser.Account{})

	config := &Config{}

	config.Router = gin.Default()
	// add CORS
	cfg := cors.DefaultConfig()
	cfg.AllowAllOrigins = true
	cfg.AllowCredentials = true
	cfg.AllowMethods = []string{"GET", "POST", "PUT", "PATCH"}
	cfg.AllowHeaders = []string{"Authorization", "Origin", "Accept", "X-Requested-With", " Content-Type", "Access-Control-Request-Method", "Access-Control-Request-Headers"}
	config.Router.Use(cors.New(cfg))

	config.Db = db
	return config
}

func (config *Config) Listen(port string) {
	config.Router.Run(port)
}

func (config *Config) AssignHandler(route string, handler Handler) {
	config.Router.GET(route, func(context *gin.Context) {
		handler(&Ctx{
			Config:  config,
			Context: context,
		})
	})
	config.Router.POST(route, func(context *gin.Context) {
		handler(&Ctx{
			Config:  config,
			Context: context,
		})
	})
	config.Router.PUT(route, func(context *gin.Context) {
		handler(&Ctx{
			Config:  config,
			Context: context,
		})
	})
	config.Router.PATCH(route, func(context *gin.Context) {
		handler(&Ctx{
			Config:  config,
			Context: context,
		})
	})
}
