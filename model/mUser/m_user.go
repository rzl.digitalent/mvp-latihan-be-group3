package mUser

import (
	"mvcgolang/utils"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type Account struct {
	ID       int    `gorm:"primary_key" json:"-"`
	Name     string `json:"name"`
	Password string `json:"password,omitempty"`
	Email    string `gorm:"index:,unique" json:"email"`
}

type Auth struct {
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (auth *Auth) Login(db *gorm.DB) (error, string) {
	account := Account{}
	if strings.TrimSpace(auth.Email) == "" {
		return errors.New("Email tidak boleh kosong"), ""
	}

	if err := db.Where(&Account{Email: auth.Email}).First(&account).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Account tidak dapat ditemukan"), ""
		}
	}

	if !utils.SamePassword(account.Password, auth.Password) {
		return errors.New("Password salah"), ""
	} else {
		sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"email": auth.Email,
			"id":    account.ID,
		})
		token, err := sign.SignedString([]byte("secret"))
		if err != nil {
			return err, ""
		}
		return nil, token
	}
}

func (account *Account) InsertNewAccount(db *gorm.DB) error {
	if err := db.Create(&account).Error; err != nil {
		return errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return nil
}

func (account *Account) GetAccountDetail(db *gorm.DB) error {
	idAccount := account.ID
	res := *account

	if err := db.Where(&Account{ID: idAccount}).Find(&res).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.Errorf("Account not found")
		}
		return errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	*account = res

	return nil
}
