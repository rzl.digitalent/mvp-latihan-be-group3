package main

import (
	"mvcgolang/controller"
	"mvcgolang/controller/cGuest"
	"mvcgolang/controller/cUser"
)

func main() {
	server := controller.Init()
	server.AssignHandler(`/guest/create-account`, cGuest.CreateAccount)
	server.AssignHandler(`/guest/login`, cGuest.Login)
	server.AssignHandler(`/user/account`, cUser.Account)
	server.Listen(":80")
}
